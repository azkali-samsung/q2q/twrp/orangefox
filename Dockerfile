ARG CI_REGISTRY_IMAGE

FROM ubuntu:focal

WORKDIR /OrangeFox

ENV LC_ALL="C"
ENV TZ="Etc/UTC"
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y git aria2 wget
RUN git clone https://gitlab.com/OrangeFox/sync
RUN git clone https://gitlab.com/OrangeFox/misc/scripts
RUN sed -i 's/sudo//g' scripts/setup/android_build_env.sh
RUN sed -i 's/sudo//g' scripts/setup/make.sh
RUN bash ./scripts/setup/android_build_env.sh